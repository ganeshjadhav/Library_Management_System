CREATE TABLE `admin` (
	`AdminID` INT NOT NULL AUTO_INCREMENT,
	`Password` varchar(100) NOT NULL,
	PRIMARY KEY (`AdminID`)
);

CREATE TABLE `UserTable` (
	`UserID` BINARY NOT NULL AUTO_INCREMENT,
	`Email` varchar(100) NOT NULL,
	`Full Name` varchar(100) NOT NULL,
	`Address` varchar(100) NOT NULL,
	`Phone Number` varchar(20) NOT NULL,
	`Password` varchar(100) NOT NULL,
	`Secret Question ID` INT NOT NULL,
	`Address Flag` BINARY NOT NULL DEFAULT '0',
	`TotFine` INT(32) NOT NULL DEFAULT '0',
	PRIMARY KEY (`UserID`)
);

CREATE TABLE `BookTable` (
	`Bid` INT NOT NULL AUTO_INCREMENT,
	`ISBN Number` varchar(50) NOT NULL UNIQUE,
	`Bname` varchar(100) NOT NULL,
	`AuthorName` varchar(100) NOT NULL,
	`AvailableQuantity` INT NOT NULL,
	`IssuedQuantity` INT NOT NULL UNIQUE DEFAULT '0',
	`Description` varchar(500) NOT NULL,
	`ImgPath` varchar(200) NOT NULL,
	PRIMARY KEY (`Bid`)
);

CREATE TABLE `Issued Books` (
	`BookId` INT NOT NULL DEFAULT '0',
	`Uid` INT NOT NULL DEFAULT '0',
	`IssuedDate` DATE NOT NULL
);

CREATE TABLE `ReturnedBooks` (
	`BookID` INT NOT NULL DEFAULT '0',
	`Uid` INT NOT NULL,
	`IssuedDate` DATE NOT NULL,
	`Returned Date` DATE NOT NULL
);

ALTER TABLE `Issued Books` ADD CONSTRAINT `Issued Books_fk0` FOREIGN KEY (`BookId`) REFERENCES `BookTable`(`Bid`);

ALTER TABLE `Issued Books` ADD CONSTRAINT `Issued Books_fk1` FOREIGN KEY (`Uid`) REFERENCES `UserTable`(`UserID`);

ALTER TABLE `ReturnedBooks` ADD CONSTRAINT `ReturnedBooks_fk0` FOREIGN KEY (`BookID`) REFERENCES `BookTable`(`Bid`);

ALTER TABLE `ReturnedBooks` ADD CONSTRAINT `ReturnedBooks_fk1` FOREIGN KEY (`Uid`) REFERENCES `UserTable`(`UserID`);

